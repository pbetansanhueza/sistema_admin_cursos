import {Request, Response} from 'express';
import pool from '../database';

class CursosController{

    public async list(req: Request,res: Response){
        const cursos = await pool.query('select * from cursos');
        res.json(cursos); 
    }

    public async listOne(req: Request,res: Response): Promise <any> {
        const {id}  = req.params;
        const curso = await pool.query('select * from cursos where id = '+id+'');
        
        if(curso.length > 0){
            return res.json(curso[0]); 
        } 

       res.status(404).json({ text: "No se encontraron registros."});
    }

    public async create(req: Request,res: Response): Promise <void> {
        await pool.query('insert into cursos set ?', [req.body]);
        res.json({ codigo: 200,  message : 'Curso creado con éxito'}); 
    }

    public async delete(req: Request,res: Response):Promise <void> {
        const {id}  = req.params;
        await pool.query('delete from cursos where id = '+id+'');
        res.json({ codigo: 200, message : "El curso ha sido eliminado con éxito"});
    }

    public async update(req: Request,res: Response): Promise <void> {
        const {id}  = req.params;
        await pool.query('UPDATE cursos set ? where id = ?', [req.body,id]);
        res.json({ codigo: 200, message: "Curso modificado con éxito"});
    }
}

const cursosController = new CursosController();
export default cursosController;