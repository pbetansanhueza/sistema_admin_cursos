import {Router} from 'express';
import cursosController from '../controllers/cursosController';

class CursosRoutes{

    public router: Router = Router();

    constructor(){
        this.config();
    }

    config():void{
        this.router.get('/listar', cursosController.list);
        this.router.get('/listarUno/:id', cursosController.listOne);
        this.router.post('/crear', cursosController.create);
        this.router.put('/modificar/:id', cursosController.update);
        this.router.delete('/borrar/:id', cursosController.delete);
    }
}

const cursosRoutes = new CursosRoutes();
export default cursosRoutes.router;