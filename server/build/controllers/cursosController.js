"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const database_1 = __importDefault(require("../database"));
class CursosController {
    list(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const cursos = yield database_1.default.query('select * from cursos');
            res.json(cursos);
        });
    }
    listOne(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id } = req.params;
            const curso = yield database_1.default.query('select * from cursos where id = ' + id + '');
            if (curso.length > 0) {
                return res.json(curso[0]);
            }
            res.status(404).json({ text: "No se encontraron registros." });
        });
    }
    create(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            yield database_1.default.query('insert into cursos set ?', [req.body]);
            res.json({ codigo: 200, message: 'Curso creado con éxito' });
        });
    }
    delete(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id } = req.params;
            yield database_1.default.query('delete from cursos where id = ' + id + '');
            res.json({ codigo: 200, message: "El curso ha sido eliminado con éxito" });
        });
    }
    update(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id } = req.params;
            yield database_1.default.query('UPDATE cursos set ? where id = ?', [req.body, id]);
            res.json({ codigo: 200, message: "Curso modificado con éxito" });
        });
    }
}
const cursosController = new CursosController();
exports.default = cursosController;
