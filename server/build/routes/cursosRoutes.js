"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const cursosController_1 = __importDefault(require("../controllers/cursosController"));
class CursosRoutes {
    constructor() {
        this.router = express_1.Router();
        this.config();
    }
    config() {
        this.router.get('/listar', cursosController_1.default.list);
        this.router.get('/listarUno/:id', cursosController_1.default.listOne);
        this.router.post('/crear', cursosController_1.default.create);
        this.router.put('/modificar/:id', cursosController_1.default.update);
        this.router.delete('/borrar/:id', cursosController_1.default.delete);
    }
}
const cursosRoutes = new CursosRoutes();
exports.default = cursosRoutes.router;
