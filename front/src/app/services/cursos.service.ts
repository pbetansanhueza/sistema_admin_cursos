import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Curso } from '../models/Curso';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CursosService {

  API_URI = 'http://localhost:3000/api/cursos'; 

  constructor(private http: HttpClient) { }

  listCursos(){
    return this.http.get(`${this.API_URI}/listar`)
  }

  listCurso(id: string){
    return this.http.get(`${this.API_URI}/listarUno/${id}`);
  }

  deleteCurso(id: string){
    return this.http.delete(`${this.API_URI}/borrar/${id}`);
  }

  createCurso(curso:Curso){
    return this.http.post(`${this.API_URI}/crear`, curso);
  }

  updateCurso(id: string, updatedCurso: Curso): Observable<any> {
    return this.http.put(`${this.API_URI}/modificar/${id}`, updatedCurso);
  }
}
