import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CursosListComponent } from './components/cursos-list/cursos-list.component';
import { CursosFormComponent } from './components/cursos-form/cursos-form.component';

const routes: Routes = [
  {
    path: '',
    redirectTo : '/cursos',
    pathMatch: 'full'
  },
  {
    path: 'cursos',
    component : CursosListComponent
  },
  {
    path:'cursos/crear',
    component: CursosFormComponent
  },
  {
    path:'cursos/modificar/:id',
    component: CursosFormComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
