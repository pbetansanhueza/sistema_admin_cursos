import { Component, OnInit } from '@angular/core';
import { CursosService } from '../../services/cursos.service';

@Component({
  selector: 'app-cursos-list',
  templateUrl: './cursos-list.component.html',
  styleUrls: ['./cursos-list.component.css']
})
export class CursosListComponent implements OnInit {

  cursos: any = [];

  constructor(private cursosService: CursosService) { }

  ngOnInit(): void {
    this.getCursos();
  }

  getCursos(){
    this.cursosService.listCursos().subscribe(
      res =>  {
        this.cursos = res;
        console.log(this.cursos);
      },
      err => console.error(err)
    );
  }

  deleteCurso(id:string){
    this.cursosService.deleteCurso(id).subscribe(
      res =>  {
        this.getCursos();
      },
      err => console.error(err)
    );
  }

}
