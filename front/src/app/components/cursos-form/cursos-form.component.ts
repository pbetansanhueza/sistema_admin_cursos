import { Component, OnInit, HostBinding } from '@angular/core';
import { Curso } from 'src/app/models/Curso';
import { CursosService} from '../../services/cursos.service';
import { ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-cursos-form',
  templateUrl: './cursos-form.component.html',
  styleUrls: ['./cursos-form.component.css']
})
export class CursosFormComponent implements OnInit {

  @HostBinding('class') clases = 'row';

  curso: Curso = {
    id: '',
    nombre : ''
  }

  edit: boolean = false;

  constructor(private cursoService: CursosService,private router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
      const params = this.activatedRoute.snapshot.params;

      if(params.id){
        this.cursoService.listCurso(params.id).subscribe(
          res => {
            console.log(res);
            this.curso = res;
            this.edit = true;
          },
          err => console.error(err)
        );
      }
  }

  guardarCurso(){
    this.cursoService.createCurso(this.curso).subscribe(
       res => {
         this.router.navigate(['/cursos']);
       },
       err => console.error(err)
    );
  }

  updateCurso(){
    const params = this.activatedRoute.snapshot.params;
    this.cursoService.updateCurso(params.id,this.curso).subscribe(
      res => {
        this.router.navigate(['/']);
      },
      err => console.error(err)
    );
  }

}
