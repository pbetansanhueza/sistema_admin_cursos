import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule} from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavigationComponent } from './components/navigation/navigation.component';
import { CursosFormComponent } from './components/cursos-form/cursos-form.component';
import { CursosListComponent } from './components/cursos-list/cursos-list.component';
import { CursosService } from './services/cursos.service';

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    CursosFormComponent,
    CursosListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [
    CursosService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
