-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 02-03-2020 a las 16:01:33
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.2.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `sistema_administracion_cursos`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alumnos`
--

CREATE TABLE `alumnos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `alumnos`
--

INSERT INTO `alumnos` (`id`, `nombre`) VALUES
(1, 'Alum Ana'),
(2, 'Alum Aristoteles'),
(3, 'Alum Belgica'),
(4, 'Alum Braulio'),
(5, 'Alum Carla'),
(6, 'Alum Cesar'),
(7, 'Alum Daniela'),
(8, 'Alum Diego'),
(9, 'Alum Eulalia'),
(10, 'Alum Ernesto');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cursos`
--

CREATE TABLE `cursos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `cursos`
--

INSERT INTO `cursos` (`id`, `nombre`) VALUES
(1, 'Matemáticas'),
(2, 'Lenguaje'),
(3, 'Historia'),
(4, 'Ciencias'),
(5, 'Música'),
(6, 'Artes Manuales'),
(7, 'Deportes'),
(8, 'Religión'),
(9, 'Química'),
(10, 'Computación');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cursos_alumnos`
--

CREATE TABLE `cursos_alumnos` (
  `id` int(11) NOT NULL,
  `curso_id` int(11) NOT NULL,
  `alumno_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `cursos_alumnos`
--

INSERT INTO `cursos_alumnos` (`id`, `curso_id`, `alumno_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 1, 4),
(5, 1, 5),
(6, 1, 6),
(7, 1, 7),
(8, 1, 8),
(9, 1, 9),
(10, 1, 10),
(11, 2, 1),
(12, 2, 2),
(13, 2, 3),
(14, 2, 4),
(15, 2, 5),
(16, 2, 6),
(17, 2, 7),
(18, 2, 8),
(19, 2, 9),
(20, 2, 10),
(21, 3, 1),
(22, 3, 2),
(23, 3, 3),
(24, 3, 4),
(25, 3, 5),
(26, 3, 6),
(27, 3, 7),
(28, 3, 8),
(29, 3, 9),
(30, 3, 10);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cursos_profesor`
--

CREATE TABLE `cursos_profesor` (
  `id` int(11) NOT NULL,
  `curso_id` int(11) NOT NULL,
  `profesor_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `cursos_profesor`
--

INSERT INTO `cursos_profesor` (`id`, `curso_id`, `profesor_id`) VALUES
(1, 1, 1),
(2, 2, 2),
(3, 3, 3),
(4, 4, 4),
(5, 5, 5),
(6, 6, 6),
(7, 7, 7),
(8, 8, 8),
(9, 9, 9),
(10, 10, 10);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `notas`
--

CREATE TABLE `notas` (
  `id` int(11) NOT NULL,
  `prueba_id` int(11) NOT NULL,
  `alumno_id` int(11) NOT NULL,
  `nota` varchar(255) NOT NULL,
  `fecha` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `notas`
--

INSERT INTO `notas` (`id`, `prueba_id`, `alumno_id`, `nota`, `fecha`) VALUES
(1, 1, 1, '70', '2020-02-03'),
(2, 2, 1, '45', '2020-02-04'),
(3, 3, 1, '60', '2020-02-12'),
(4, 4, 1, '42', '2020-02-13'),
(5, 5, 1, '49', '2020-02-17'),
(6, 6, 1, '68', '2020-02-14'),
(7, 7, 1, '54', '2020-02-15'),
(8, 8, 1, '62', '2020-02-16'),
(9, 9, 1, '45', '2020-02-23'),
(10, 10, 1, '56', '2020-02-24'),
(11, 11, 1, '63', '2020-02-27'),
(12, 12, 1, '61', '2020-02-25'),
(13, 13, 1, '35', '2020-02-28'),
(14, 14, 1, '41', '2020-02-28'),
(15, 15, 1, '21', '2020-02-29'),
(16, 16, 1, '38', '2020-02-29'),
(17, 17, 1, '46', '2020-02-29'),
(18, 18, 1, '59', '2020-02-29'),
(19, 19, 1, '21', '2020-02-29'),
(20, 20, 1, '70', '2020-02-29'),
(44, 1, 2, '65', '2020-02-29'),
(45, 2, 2, '47', '2020-02-29'),
(46, 3, 2, '33', '2020-02-29'),
(47, 4, 2, '57', '2020-02-29'),
(48, 5, 2, '21', '2020-02-29'),
(49, 6, 2, '68', '2020-02-29'),
(50, 7, 2, '54', '2020-02-29'),
(51, 8, 2, '62', '2020-02-29'),
(52, 9, 2, '45', '2020-02-29'),
(53, 10, 2, '56', '2020-02-29'),
(54, 11, 2, '41', '2020-02-29'),
(55, 12, 2, '61', '2020-02-29'),
(56, 13, 2, '64', '2020-02-29'),
(57, 14, 2, '41', '2020-02-29'),
(58, 15, 2, '21', '2020-02-29'),
(59, 16, 2, '38', '2020-02-29'),
(60, 17, 2, '10', '2020-02-29'),
(61, 18, 2, '23', '2020-02-29'),
(62, 19, 2, '25', '2020-02-29'),
(63, 20, 2, '10', '2020-02-29'),
(64, 1, 3, '10', '2020-02-29'),
(65, 2, 3, '11', '2020-02-29'),
(66, 3, 3, '12', '2020-02-29'),
(67, 4, 3, '13', '2020-02-29'),
(68, 5, 3, '14', '2020-02-29'),
(69, 6, 3, '15', '2020-02-29'),
(70, 7, 3, '16', '2020-02-29'),
(71, 8, 3, '17', '2020-02-29'),
(72, 9, 3, '18', '2020-02-29'),
(73, 10, 3, '19', '2020-02-29'),
(74, 11, 3, '20', '2020-02-29'),
(75, 12, 3, '21', '2020-02-29'),
(76, 13, 3, '22', '2020-02-29'),
(77, 14, 3, '23', '2020-02-29'),
(78, 15, 3, '24', '2020-02-29'),
(79, 16, 3, '25', '2020-02-29'),
(80, 17, 3, '26', '2020-02-29'),
(81, 18, 3, '27', '2020-02-29'),
(82, 19, 3, '28', '2020-02-29'),
(144, 20, 3, '29', '2020-02-29');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `profesores`
--

CREATE TABLE `profesores` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `profesores`
--

INSERT INTO `profesores` (`id`, `nombre`) VALUES
(1, 'Profesor Juan'),
(2, 'Profesor Pedro'),
(3, 'Profesor Alberto'),
(4, 'Profesor Bastian'),
(5, 'Profesor Carlos'),
(6, 'Profesor Damian'),
(7, 'Profesor Esteban'),
(8, 'Profesor Fernando'),
(9, 'Profesor Gerardo'),
(10, 'Profesor Hernan');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pruebas`
--

CREATE TABLE `pruebas` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `curso_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `pruebas`
--

INSERT INTO `pruebas` (`id`, `nombre`, `curso_id`) VALUES
(1, 'Prueba Matemáticas 1', 1),
(2, 'Prueba Matemáticas 2', 1),
(3, 'Prueba Lenguaje 1', 2),
(4, 'Prueba Lenguaje 2', 2),
(5, 'Prueba Historia 1', 3),
(6, 'Prueba Historia 2', 3),
(7, 'Prueba Ciencias 1', 4),
(8, 'Prueba Ciencias 2', 4),
(9, 'Prueba Música 1', 5),
(10, 'Prueba Música 2', 5),
(11, 'Prueba Artes Manuales 1', 6),
(12, 'Prueba Artes Manuales 2', 6),
(13, 'Prueba Deportes 1', 7),
(14, 'Prueba Deportes 2', 7),
(15, 'Prueba Religión 1', 8),
(16, 'Prueba Religión 2', 8),
(17, 'Prueba Química 1', 9),
(18, 'Prueba Química 2', 9),
(19, 'Prueba Computación 1', 10),
(20, 'Prueba Computación 2', 10);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `alumnos`
--
ALTER TABLE `alumnos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cursos`
--
ALTER TABLE `cursos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cursos_alumnos`
--
ALTER TABLE `cursos_alumnos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cursos_profesor`
--
ALTER TABLE `cursos_profesor`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `notas`
--
ALTER TABLE `notas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `profesores`
--
ALTER TABLE `profesores`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `pruebas`
--
ALTER TABLE `pruebas`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `alumnos`
--
ALTER TABLE `alumnos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `cursos`
--
ALTER TABLE `cursos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `cursos_alumnos`
--
ALTER TABLE `cursos_alumnos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT de la tabla `cursos_profesor`
--
ALTER TABLE `cursos_profesor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `notas`
--
ALTER TABLE `notas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=281;

--
-- AUTO_INCREMENT de la tabla `profesores`
--
ALTER TABLE `profesores`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `pruebas`
--
ALTER TABLE `pruebas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
